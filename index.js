var photosWrapper = document.querySelector('.main-photo__wrapper')
var frameDesc = document.querySelectorAll('.main-photo .section-desc');
var titleDesc = document.querySelector('.title');
var photosOverlay = document.querySelector('.photos-frame .frame__overlay');
var currentPhotos = document.querySelectorAll('.main-photo');
var mobileDetect = document.querySelector('.mobile__detect');
var row = document.querySelectorAll('.main-photo__wrapper .main-photo__row');
var mobilePhoto = document.querySelector('.mobile__photo');
var defaultPhotosImgArray = ["img/main-photos/mount1e.jpg","img/main-photos/mount2e.jpg","img/main-photos/mount3e.jpg","img/main-photos/mount4e.jpg",
    "img/main-photos/mount5-reve.jpg","img/main-photos/mount6e.jpg","img/main-photos/mount7e.jpg","img/main-photos/mount8e.jpg","img/main-photos/mount9e.jpg"];
var currentPhotosImg = document.querySelectorAll('.main-photo .main-photo__image');
var currentSet = 0;
var initial;
var mobilInitial = 0;
var closeFrames = document.querySelectorAll('.frame__close');


// check if mobile-view
function isMobile() {
    const mobileVisible = getComputedStyle(mobileDetect).getPropertyValue('display');
    return mobileVisible === 'block';
}

function removeRows() {
    var fragment = document.createDocumentFragment();
    // var elements = row;
    row.forEach(function (element) {
        while(element.firstChild) {
            fragment.appendChild(element.firstChild);
        }
        element.parentNode.replaceChild(fragment, element);
    })
}

function removeFrame() {
    currentPhotos.forEach(function (frame) {
        if (!frame.classList.contains('frame')) {
            frame.style.display = "none";
        }
    })
}

function getVisibleFrames() {
    let currentPhotosArray = Array.from(currentPhotos);
    currentPhotosArray.sort(compare);
    let currentFrames = currentPhotosArray.filter(function(frame) {
        return frame.classList.contains('frame');
    });
    return currentFrames;
}

function activeFirstFrame() {
    let activeFrame = getVisibleFrames()[0];
    activeFrame.classList.add('active');
}

/*// init desktop rotate
function rotateDesktop() {
   window.addEventListener('load', function() {
    setTimeout(function(){
        photosWrapper.classList.remove('rotate');
    }, 2000);
})
}*/

if (isMobile()) {
    removeRows();
    removeFrame();
    activeFirstFrame();
} else {
    for (let i = 0; i < currentPhotosImg.length -1 ; i++) {
        currentPhotosImg[i].src = defaultPhotosImgArray[i];
    }
    // rotateDesktop();
}

// mobil swipe and touch-events
let threshold = 75;
let touchstartX = 0;
let touchstartY = 0;
let touchendX = 0;
let touchendY = 0;

function handleGesture() {
    var distX = touchendX - touchstartX;
    if (touchendX <= touchstartX && Math.abs(distX) >= threshold) {
        getPhotos(makePhotosArray, 999); //oprav?
    }
    // if (touchendX >= touchstartX && Math.abs(distX) >= threshold) { }
}

mobilePhoto.addEventListener('touchmove', function (event){
    event.preventDefault() // prevent scrolling when inside DIV
}, false);

mobilePhoto.addEventListener('touchend', function (event) {
    touchendX = event.changedTouches[0].screenX;
    touchendY = event.changedTouches[0].screenY;
    handleGesture();
}, false);

mobilePhoto.addEventListener('touchstart', function (event) {
    touchstartX = event.changedTouches[0].screenX;
    touchstartY = event.changedTouches[0].screenY;
}, false);



// managing sections - functions
function compare(a, b) {
  if (getComputedStyle(a).getPropertyValue('order') < getComputedStyle(b).getPropertyValue('order')){return -1;}
  if (getComputedStyle(a).getPropertyValue('order') > getComputedStyle(b).getPropertyValue('order')){return 1;}
  return 0;
}

function openSection(clicked, event) {
    const overlay = event.target.children[0];
    if (overlay.classList.contains('frame__overlay')) {
        overlay.style.display = "block";
        frameDesc.forEach(function(desc) {
            desc.style.display = "none";
        });
        titleDesc.style.display = "none";
    }
    photosWrapper.classList.add('section');
    if (isMobile()) {
        if (overlay.classList.contains('frame__overlay')) {
            overlay.style.display = "block";
        }
    }
}

// insert mobile overlay
function winOverlay(eventTarget) {
    eventTarget.children[0].classList.add('overlay');
}

// photos section
function openSectionPhoto(clicked) {
    photosOverlay.style.display = "block";
    if (isMobile()) {
        getPhotos(makePhotosArray, clicked);
    } else {
        frameDesc.forEach(function(desc) {
            desc.style.display = "none";
        });
        titleDesc.style.display = "none";
        photosWrapper.classList.add('section-photo');
        getPhotos(makePhotosArray, clicked);
    }
}

// retrieve JSON data - photos
async function getData(url) {
    const response = await fetch(url);
    return response.json()
}

async function getPhotos(callback, exception) {
    const data = await getData('/photos.json');
    callback(data.photos, exception);
}

function makePhotosArray(data, exception) {
    let thumbsaArray = data;
    insertPhotos(exception, thumbsaArray);
}

// insert photos content
function insertPhotos(exception, thumbsaArray) {
    if (isMobile()) {
        if (mobilInitial === 0) {
            mobilePhoto.children[0].setAttribute('src', thumbsaArray[0]);
            mobilePhoto.children[1].setAttribute('src', thumbsaArray[1]);
            mobilInitial = 2;
        } else {
            if (mobilInitial < thumbsaArray.length) {
                mobilePhoto.children[0].setAttribute('src', thumbsaArray[mobilInitial]);
                mobilePhoto.children[1].setAttribute('src', thumbsaArray[mobilInitial+1]);
                mobilInitial = mobilInitial + mobilePhoto.children.length;
            } else {
                mobilePhoto.children[0].setAttribute('src', thumbsaArray[0]);
                mobilePhoto.children[1].setAttribute('src', thumbsaArray[1]);
                mobilInitial = 2;
            }
        }
    } else {
        var currentPhotosImgArray = Array.from(currentPhotosImg);
        currentPhotosImgArray.splice(exception, 1);
/*        currentPhotos.forEach(function (frame) {
            frame.classList.add("transition");
        })*/
        if (initial !== undefined) {
            for (var i = 0; i < currentPhotosImgArray.length; i++) {
                if (thumbsaArray.length > (i+ (initial*currentSet))) {
                    currentPhotosImgArray[i].src = thumbsaArray[i+ (initial*currentSet)];
                    if ((i+1 + (initial*currentSet)) % thumbsaArray.length === 0) {
                        currentSet = 0;
                        initial = undefined;
                        return
                    }
                }
            }
        } else {
            for (var i = 0; i < currentPhotosImgArray.length; i++) {
                currentPhotosImgArray[i].src = thumbsaArray[i];
            }
        }
        currentSet += 1;
        initial = i;
    }
}

// event-listeners
photosWrapper.addEventListener('click', function(event) {
    console.log('eee', event.target);
    if (event.target.classList.contains("move-it")) {
        moveOrder(currentPhotos);
    }
    var clickedFrame = event.target.dataset.frameIndex;
    var clickedFrameParent = event.target.parentElement.parentElement.dataset.frameIndex;
    if (isMobile()) {
        const mobileOverlayDisplay = getComputedStyle(photosOverlay).getPropertyValue('display');
        if (mobileOverlayDisplay === "none" && (Number(clickedFrame) === 6)) {
            winOverlay(event.target);
            openSectionPhoto(clickedFrame);
        } else if (mobileOverlayDisplay === 'block' && (Number(clickedFrameParent) === 6) && event.target.classList.contains('next')) {
            getPhotos(makePhotosArray, clickedFrame);
        } else if (mobileOverlayDisplay === 'none' && (Number(clickedFrame) !== 6)) {
            winOverlay(event.target);
            openSection(Number(clickedFrame), event);
        }
    } else {
        if (!photosWrapper.classList.contains('section') && !photosWrapper.classList.contains('section-photo') &&
            (event.target.children[0].classList.contains('frame__overlay')) && (Number(clickedFrame) !== 6)) {
            openSection(Number(clickedFrame), event);
        }
        if (!photosWrapper.classList.contains('section') && Number(clickedFrame) === 6) {
            openSectionPhoto(Number(clickedFrame));
            photosWrapper.classList.add('section-photo');
        }
        if (photosWrapper.classList.contains('section-photo') && (Number(clickedFrame) !== 6)) {
            showModal(event);
        }
    }
});

// closing functions
function closeSection(section) {
    returnToDefault();
    section.parentElement.style.display = "none";
    photosWrapper.classList.remove('section');
    photosWrapper.classList.remove('section-photo');
    frameDesc.forEach(function(desc) {
        desc.style.display = "block";
    });
    titleDesc.style.display = "block";
    currentSet = 0;
    mobilInitial = 0;
}
function returnToDefault() {
    if (!isMobile()) {
        for (let i = 0; i < currentPhotosImg.length ; i++) {
            console.log('cus', defaultPhotosImgArray[i]);
            currentPhotosImg[i].src = defaultPhotosImgArray[i];
        }
    }
}
// closing event-listeners
closeFrames.forEach(function(frame){
    frame.addEventListener('click', function(event) {
        event.stopPropagation(); // ked mam, tak klika aj photosFrame
        closeSection(frame);
    });
})

// modal window variables
var modal = document.querySelector('.gallery__modal');
var image = document.querySelector(".modal__img");

// modal window function
function showModal(e) {
    if (e.target.classList.contains('main-photo')) {
        var targetImage = e.target.querySelector('.main-photo__image');
        modal.style.display = "block";
        var fullimagePath = targetImage.src.replace('/img/', '/img/full-for-web/');
        fullimagePath = fullimagePath.replace('.jpg', '-full.jpg');
        image.src = fullimagePath;
    }
}

// modal window event-listener
modal.addEventListener('click', function(){
    modal.style.display = "none";
});


// srandicky - mix mountains - move order
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}
function moveOrder(photos) {
    var arr = [...Array((photos.length+1)).keys()];
    shuffleArray(arr); // shuffle indexy
    var i = 0;
    photos.forEach(function(value) {
        value.style.order = arr[i];
        i++;
    });
}
